<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

?>

<?php include("../src/Views/header.php"); ?>

<?php include("../src/Views/nav.php"); ?>

  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br>
      <a href="<?= PUBLIC_PATH; ?>bug" class="waves-effect waves-light btn-flat blue-grey-text"><i class="material-icons left">chevron_left</i>Retour à la liste</a>
      <a style="float:right;" href="<?=PUBLIC_PATH?>bug/update/<?= $bug->getId() ?>" class="waves-effect waves-light btn-flat blue-grey-text"><i class="material-icons left">edit</i>Edit</a>
      <br>
      <h3 class="blue-grey-text text-darken-4">Fiche description incident</h3>
    </div>

    </div>

  <br>
  <div class="container">

    <div class="section">

      <div class="row">
      <div class="col s6">
        <label for="nom">Nom de l'incident</label>
        <p><?=$bug->getTitle();?></p>
      </div>
      <div class="col s6">
        <label for="date">Date</label>
          <p><?php echo $bug->getCreatedAt()->format("d/m/Y");?></p>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <label for="description">Description</label>
        <p style="text-align: justify;"><?=$bug->getDescription();?></p>
      </div>
    </div>

    </div>
    <br><br>
  </div>

    <?php include("../src/Views/footer.php"); ?>
