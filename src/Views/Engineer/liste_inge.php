<?php

    /** @var $bug \BugApp\Models\Bug */

    $bugs = $parameters['bugs'];

?>

<?php include("../src/Views/header.php"); ?>

<?php include("../src/Views/nav.php"); ?>

  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br><br>
      <h3 class="blue-grey-text text-darken-4">Liste des incidents</h3>
    </div>

    </div>

  <br>
  <div class="container">

    <div class="section">
      <form>
        <p>
          Filtres
        </p>
          <p>
            <label>
              <input type="checkbox" class="filled-in"  id="non-cloture"/>
              <span>Afficher uniquement les incidents non-cloturés</span>
            </label>
          </p>
          <p>
                <label>
                  <input type="checkbox" class="filled-in" id="affiche-assigner" />
                  <span>Afficher uniquement les incidents que je me suis assignés</span>
                </label>
              </p>
        </form>

      <table id="table" class="striped">
        <thead>
          <tr>
              <th>id</th>
              <th>Sujet</th>
              <th>Utilisateur</th>
              <th>Date</th>
              <th>Détail</th>
              <th>Ingénieur</th>
              <th></th>
          </tr>
        </thead>

        <tbody>
        <?php foreach($bugs as $bug) {  ?>
          <tr>
            <td><?= $bug->getId();?></td>
            <td><?= $bug->getTitle();?></td>
            <td><?= $bug->getRecorder();?></td>
            <td><?php echo $bug->getCreatedAt()->format("d/m/Y");?></td>
            <td><a class="waves-effect waves-light btn blue-grey" href="<?= PUBLIC_PATH; ?>bug/show/<?=$bug->getId();?>" /><i class="material-icons left">add</i>Afficher</a></td>
            <td>
              <?php if($bug->getEngineer() != null){ ?>
              <button class="waves-effect waves-light btn blue-grey"><i class="material-icons left">person</i><?= $bug->getEngineer(); ?></button>
              <?php }else{ ?>
              <button class="waves-effect waves-light btn blue-grey" value="<?= $bug->getId();?>" id="assigner"><i class="material-icons left">person_add</i>Assigner</button>
              <?php } ?>
            </td>
            <td>
              <?php if($bug->getClosedAt() != null){ ?>
              <button class="waves-effect waves-light btn blue-grey disabled">Clôturer</button>
              <?php }else{ ?>
              <button class="waves-effect waves-light btn blue-grey" value="<?= $bug->getId();?>" id="cloturer"><i class="material-icons left">close</i>Clôturer</button>
              <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>

      </table>

    </div>
    <br><br>
  </div>


  <?php include("../src/Views/footer.php"); ?>
