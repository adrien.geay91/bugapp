<?php include("../src/Views/header.php"); ?>

<?php include("../src/Views/nav.php"); ?>

  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br><br>
      <h3 class="blue-grey-text text-darken-4">Rapport incident</h3>
    </div>

    </div>

  <br>
  <div class="container">

    <div class="section">

      <form class="col s12" action="#" method="post">
        <div class="row">
        <div class="input-field col s6">
          <input placeholder="" id="title" name="title" type="text" class="validate" required>
          <label for="nom">Nom de l'incident</label>
        </div>
        <div class="input-field col s6">
          <input placeholder="" id="date" type="date" name="createdAt" class="validate" required>
          <label for="date">Date</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <textarea placeholder="" id="description" name="description" class="materialize-textarea" type="text" required></textarea>
          <label for="description">Description</label>
        </div>
      </div>
      <input style="float:right;" class="waves-effect waves-light btn blue-grey" type="submit" name="submit" value="Ajouter">
      </form>
      <br>
    </div>
    <br><br>
  </div>

  <?php include("../src/Views/footer.php"); ?>
