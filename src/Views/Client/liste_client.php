<?php

    /** @var $bug \BugApp\Models\Bug */

    $bugs = $parameters['bugs'];

?>

<?php include("../src/Views/header.php"); ?>

<?php include("../src/Views/nav.php"); ?>

  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br><br>
      <h3 class="blue-grey-text text-darken-4">Liste des incidents</h3>
    </div>

    </div>

  <br>
  <div class="container">

  <div><a href="<?= PUBLIC_PATH; ?>bug/add" class="btn-floating btn-large waves-effect waves-light blue-grey"><i class="material-icons">add</i></a> Rapporter un incident</div>

    <div class="section">

      <table class="striped">
        <thead>
          <tr>
              <th>id</th>
              <th>Sujet</th>
              <th>Date</th>
              <th>Clôture</th>
              <th>Détail</th>
          </tr>
        </thead>

        <tbody>

          <?php foreach($bugs as $bug) {  ?>

          <tr>
            <td><?= $bug->getId();?></td>
            <td><?= $bug->getTitle();?></td>
            <td><?php echo $bug->getCreatedAt()->format("d/m/Y");?></td>
            <td>
              <?php if($bug->getClosedAt() != null){

                echo $bug->getClosedAt()->format("d/m/Y");
              }else{
                echo "En Cours";
              }
              ?>
          </td>
            <td><a class="waves-effect waves-light btn blue-grey" href="<?= PUBLIC_PATH; ?>bug/show/<?=$bug->getId();?>" /><i class="material-icons left">add</i>Afficher</a></td>
          </tr>

        <?php } ?>
        </tbody>
      </table>

    </div>
    <br><br>
  </div>

<?php include("../src/Views/footer.php"); ?>
