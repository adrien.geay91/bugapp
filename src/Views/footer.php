<footer class="page-footer blue-grey">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">BugApp</h5>
        <p class="grey-text text-lighten-4">Gérez les tickets d'incident et résolvez les réclamations des clients rapidement. .</p>

      </div>

      </div>

      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
    </div>
  </div>
</footer>


<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="/js/materialize.js"></script>
<script src="/js/init.js"></script>
<script src="/js/script.js"></script>
<script src="/js/datatables.min.js"></script>
</body>
</html>
