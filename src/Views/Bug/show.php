<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

?>

<!DOCTYPE html>

<html>

<head>

</head>

<body>

<h1>Détail d'un Incident</h1>

<?php ?>

    <h2><?=$bug->getTitle();?></h2>

    <div>Description : <?=$bug->getDescription();?></div>

   <div> Date de création : <?php echo $bug->getCreatedAt()->format("d/m/Y");?></div>

    <div>
    
        <?php if($bug->getClosedAt() != null){

            echo $bug->getClosedAt()->format("d/m/Y");
        }
        ?>
        
    </div>

    <div><a href="<?= PUBLIC_PATH; ?>bug">Retour à la liste</a></div>

</body>

</html>