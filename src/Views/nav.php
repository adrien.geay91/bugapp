<nav class="blue-grey" role="navigation">
  <div class="nav-wrapper container"><a id="logo-container" href="<?= PUBLIC_PATH; ?>bug" class="brand-logo"><i class="material-icons">bug_report</i> BugApp</a>
    <ul class="right hide-on-med-and-down">

      <?php
  if(isset($_SESSION['user'])){
        $user = unserialize($_SESSION['user']);
       ?>
       <li><i class="material-icons">person</i></li>
       <li id="username"><?= $user->getNom(); ?></li>
       <li><a href="logout">logout</a></li>
     <?php  }  ?>

    </ul>

    <ul id="nav-mobile" class="sidenav">
      <li><a href="#"></li>
    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>
