<?php

if(isset( $parameters['error'])){
    $error = $parameters['error'];
}

 include("../src/Views/header.php"); ?>

    <center>
    </br>
  </br>
        <div class="z-depth-3 y-depth-3 x-depth-3 grey green-text lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px; margin-top: 100px; solid #EEE;">
<div class="section no-pad-bot" id="index-banner">
        <div class="container">

            <h1 class="header center blue-grey-text text-darken-4">Login</h1>
        </div>
    </div>

    <div class="container">
        <div class="section">

            <div class="row"><?php if(isset($error)) echo $error;?></div>

            <div class="row">
                <form class="col s12" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="" id="email" type="email" class="validate" name="email">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col s6">
                            <input placeholder=""  id="password" type="password" class="validate" name="password">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <button class="waves-effect waves-light btn blue-grey" type="submit" name="submit">Login
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</center>
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/js/materialize.js"></script>
    <script src="/js/init.js"></script>
