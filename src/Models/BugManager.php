<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);


        // Retour
        return $bug;
    }

    public function findAll()
    {

      // Récupération de tous les incidents en BDD
     $dbh = static::connectDb();

     $req_find_all = $dbh->prepare('SELECT *, B.id AS BugId, U.nom AS Ingenieur, U2.nom AS Recorder
                                    FROM bug B
                                    LEFT JOIN engineer E ON B.engineer_id = E.id
                                    LEFT JOIN user U ON E.user_id = U.id
                                    LEFT JOIN recorder R ON B.recorder_id = R.id
                                    LEFT JOIN user U2 ON R.user_id = U2.id
                                    ORDER BY createdAt DESC');
     $req_find_all->execute();

     $bugs = [];

     while($result = $req_find_all->fetch(\PDO::FETCH_ASSOC)) {
         // Instanciation d'un bug
         $bug = new Bug();
         $bug->setId($result["BugId"]);
         $bug->setTitle($result["title"]);
         $bug->setDescription($result["description"]);
         $bug->setCreatedAt($result["createdAt"]);
         $bug->setClosedAt($result["closed"]);
         $bug->setRecorder($result["Recorder"]);
         $bug->setEngineer($result["Ingenieur"]);

         array_push($bugs, $bug);
     }
        return $bugs;
    }

    public function add(Bug $bug)
    {

        // Ajout d'un incident en BDD
        $dbh = static::connectDb();

        $sql = 'INSERT INTO bug (title, description, createdAt)
                VALUES (:title, :description, :createdAt)';

        $sth = $dbh->prepare($sql);

        $sth->execute(array(

                ":title" => $bug->getTitle(),
                ":description" => $bug->getDescription(),
                ":createdAt" => $bug->getCreatedAt()->format('Y-m-d H:i:'),

        ));



    }

    public function update(Bug $bug){

        // Update d'un incident en BDD
       $dbh = static::connectDb();

       $req = $dbh->prepare('UPDATE bug
                             SET closed = :clotureDate
                             WHERE id = '.$bug->getId()
                           );

       $req->execute(array(

              'clotureDate' => date("Y-m-d H:i:s")

       ));
    }

    public function EngineerId($id){

        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT id
                              FROM engineer
                              WHERE engineer.user_id = :id_engineer'
                            );

        $sth->bindParam(':id_engineer', $id, \PDO::PARAM_INT);

        $sth->execute();

        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['id'];
    }

    public function assignation(Bug $bug, Engineer $engineer){

        // Ajout d'un incident en BDD
        $dbh = static::connectDb();

        $req = $dbh->prepare('UPDATE bug
                            SET engineer_id = :engineer
                            WHERE id ='.$bug->getId()
                            );

        $req->execute(array(

            'engineer' => $this->EngineerId($engineer->getId())

          ));

     }

}
